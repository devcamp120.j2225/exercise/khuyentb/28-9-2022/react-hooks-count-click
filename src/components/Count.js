import { useEffect, useState } from "react";

function Count () {
    const [count, setCount] = useState(0);

    const increaseCount = () => {
        console.log("increase count");
        setCount(count + 1);
    }

    
    return (
        <>
            <p>You clicked {count} times!</p>
            <button onClick={increaseCount}>Click me!</button>
        </>
        
    )
}

export default Count;